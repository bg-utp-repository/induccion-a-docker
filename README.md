# [BG-UTP] Inducción a Docker - Programa Fomento al Talento Integral TIC

## Docker
Docker es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos.​ Docker utiliza características de aislamiento de recursos del kernel Linux, tales como cgroups y espacios de nombres (namespaces) para permitir que "contenedores" independientes se ejecuten dentro de una sola instancia de Linux, evitando la sobrecarga de iniciar y mantener máquinas virtuales.

## Índice de contenido

### [0) Preparación del ambiente](00.Material/01.Informacion/00.Ambiente.md)
¿Qué necesitaremos para el curso? ¿Necesito instalar algo? Son algunas de las preguntas que responderemos en esta sección.

### [1) ¡Hola Mundo! Desde Docker](00.Material/01.Informacion/01.HelloWorld.md)
Crearemos el tradicional "Hello world" desde docker y aprenderemos a manejar nuestro primer contenedor.

### [2) Registros e imágenes](00.Material/01.Informacion/02.RegistroImagenes.md)
¿Imágenes son versiones de un contenedor? ¿Esos registros son mágicos? ¿Puedo crear mis propias imagenes a partir de otras? Trataremos de responder estas preguntas con el siguiente experimento.

### [3) Logs, puertos, volumenes y redes](00.Material/01.Informacion/03.LogsYRedes.md)
Aprendamos sobre docker log, como transferir archivos de nuestro docker a a través de volúmenes y a trabajar con redes.

### [4) DockerFile](00.Material/01.Informacion/04.DockerFile.md)
Juguemos con archivos docker file y reduzcamos las banderas necesarias para crear nuestro contenedor.

### [5) Docker compose](00.Material/01.Informacion/05.Compose.md)
Utilicemos compose para construir nuestros más de 1 contenedores.