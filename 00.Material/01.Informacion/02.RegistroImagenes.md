### [< Volver atrás](../../README.md)

# Registros e imágenes

En esta ocasión haremos algo un poco más ambicioso que escribir en pantalla un hello world. Escribiremos un hello world.... con estilo 👌.

## Instalemos Ubuntu en un cotenedor
Imaginemos que queremos instalar Ubuntu en nuestro computador. Hay varias maneras de hacerlo de manera nativa booteando, una maquina virtual o... ¿por qué no? UN CONTENEDOR!!

1) Volvemos a nuestra consola y bajaremos una imagen de ubuntu a continuación agregaremos al comando run que ya conocemos ```-ti``` que indicará al contenedor que además de levantar el contenedor de ubuntu ejecute el programa ```bash``` (terminal de ubuntu)-

```console
docker run -ti ubuntu /bin/bash
```

Podremos visualizar entonces que nos encontramos en la terminal de un Ubuntu totalmente funcional. Haremos uso de comandos propios de Ubuntu ```apt-get```  para actualizar sus dependencias y descargar Figlet un programita que permite recibir de parametro un texto y generar una especie de banner en la consola.

```console
apt-get update
```

y una vez actualizado procedemos a instalar Figlet:
```console
apt-get install figlet
```

Podemos probar que figlet se instaló correctamente utilizando el comando ```figlet [texto que colocar en el banner]``` probémoslo y salgamos de bash de ubuntu:

```console
root@ad5a5b0b0ff2:/# figlet "Hola Mundo"
 _   _       _         __  __                 _
| | | | ___ | | __ _  |  \/  |_   _ _ __   __| | ___
| |_| |/ _ \| |/ _` | | |\/| | | | | '_ \ / _` |/ _ \
|  _  | (_) | | (_| | | |  | | |_| | | | | (_| | (_) |
|_| |_|\___/|_|\__,_| |_|  |_|\__,_|_| |_|\__,_|\___/

root@ad5a5b0b0ff2:/# exit
```

Lo que acabamos de crear fue un contenedor base a partir de la imagen oficial de Ubuntu y la modificamos (actualizamos el apt-get e instalamos figlet). Podemos visualizar las imágenes que hemos descargado y que se encuentran de manera local mediante el siguiente comando:

```console
$ docker image ls

REPOSITORY    TAG       IMAGE ID       CREATED       SIZE
ubuntu        latest    7e0aa2d69a15   8 days ago    72.7MB
hello-world   latest    d1165f221234   8 weeks ago   13.3kB
```

Esto nos indica que en nuestro repositorio local de imágenes tenemos 2. La imagen oficial de ubuntu en mi caso con el id ```7e0aa2d69a15``` y la imagen de nuestro primer ejercicio ```d1165f221234```. Procederemos a revisar los últimos contenedores que hemos levantando mediante el siguiente comando: 

```console
$ docker ps -a | head

CONTAINER ID   IMAGE         COMMAND       CREATED         STATUS                     PORTS     NAMES
76a4ca71e11d   ubuntu        "/bin/bash"   3 minutes ago   Exited (0) 3 minutes ago             jovial_bohr
adcea0c91c12   hello-world   "/hello"      4 minutes ago   Exited (0) 4 minutes ago             determined_chaplygin
```

Nos podemos percatar que el ubuntu que levantamos hace poco cuenta, en mi caso, con el id ```76a4ca71e11d```. Lo que queremos hacer es guardar esta imagen, que fue modificada por nosotros, se convierta en una imagen que podamos utilizar a futuro. Esto lo podemos realizar con el siguiente comando:

```console
## Atención deberás de colocar tu propio id
$ docker commit 76a4ca71e11d

sha256:53cd8dd9820872191625431695c4885e8afc3abd53cd93c841b9cb3c661d9050
```

Si volvemos a revisar las imagenes locales nos encontraremos con lo siguiente:

```console
$ docker image ls

REPOSITORY    TAG       IMAGE ID       CREATED              SIZE
<none>        <none>    53cd8dd98208   About a minute ago   102MB
ubuntu        latest    7e0aa2d69a15   8 days ago           72.7MB
hello-world   latest    d1165f221234   8 weeks ago          13.3kB
```

No hemos colocado nombre a nuestro repositorio ni un tag (versión). Vamos a realizar este tag para poder identificar la imagen más fácilmente y no sólo por su id. Para esto utilizaremos el comando ```image tag``` seguido por el id de la imagen, nombre y versión:

```console
$ docker image tag 53cd8dd98208 miubuntu:1.0

$ docker image ls

REPOSITORY    TAG       IMAGE ID       CREATED         SIZE
miubuntu      1.0       53cd8dd98208   4 minutes ago   102MB
ubuntu        latest    7e0aa2d69a15   8 days ago      72.7MB
hello-world   latest    d1165f221234   8 weeks ago     13.3kB
```

Ahora contamos con nuestra propia imagen personalizada de ubuntu que cuenta con los cambios que relizamos. Podemos probar esta imagen de la siguiente manera:

```console
$ docker run miubuntu:1.0 figlet "hola mundo"
 _           _                                   _
| |__   ___ | | __ _   _ __ ___  _   _ _ __   __| | ___
| '_ \ / _ \| |/ _` | | '_ ` _ \| | | | '_ \ / _` |/ _ \
| | | | (_) | | (_| | | | | | | | |_| | | | | (_| | (_) |
|_| |_|\___/|_|\__,_| |_| |_| |_|\__,_|_| |_|\__,_|\___/
```

Si realizamos este mismo comando con la imagen de ubuntu oficial nos daremos cuenta que nos arrojará un error. Esto es porque la imagen de ubuntu oficial no cuenta con figlet de manera nativa. Solo la imagen modificada por nosotros lo hace.

```console
$ docker run ubuntu figlet "hola mundo"

docker: Error response from daemon: OCI runtime create failed: container_linux.go:370: starting container process caused: exec: "figlet": executable file not found in $PATH: unknown.
ERRO[0000] error waiting for container: context canceled
```

Un último comando de mucha utilidad que podrás utilizar es ```docker container ls``` que lista todos los contenedores que se encuentran vivos. Recuerda el contenedor se mantendrá "vivo" mientras se esté ejecutando. Ejemplo:

```console
$ docker container ls

CONTAINER ID   IMAGE     COMMAND       CREATED          STATUS          PORTS     NAMES
ea6541a25a67   ubuntu    "/bin/bash"   18 seconds ago   Up 17 seconds             quizzical_mclean
```

### [< Volver atrás](../../README.md)
