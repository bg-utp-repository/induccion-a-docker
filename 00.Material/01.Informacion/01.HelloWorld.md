### [< Volver atrás](../../README.md)

# ¡Hola Mundo! Desde Docker

Si no lo haz hecho ingresa a la [https://labs.play-with-docker.com/](https://labs.play-with-docker.com/) con tu cuenta de docker.

## Comandos útiles
```console
# Revisar versión de docker
$ docker --version

# Comandos comunes
$ docker --help
```

## Pasos a seguir
* Haz clic sobre ```ADD NEW INSTANCE```
* A continuación te aparezará una consola en ella escribirás el siguiente comando:

```console
docker run hello-world
```

Docker te deberá contestar algo como esto:

```console
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
b8dfde127a29: Pull complete
Digest: sha256:f2266cbfc127c960fd30e76b7c792dc23b588c0db76233517e1891a4e357d519
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

## ¿Qué sucedió?

El comando run es uno de los más básicos comandos de docker. Este comando sirve para inicializar un contenedor dándole como parámetro el nombre de la imágen.

* Al dar esta instrucción el demonio de docker intentó realizar la búsqueda de esta imagen de manera local para inicializarla. 
* Al no encontrala descargó la imagen del docker con este nombre directamente desde Docker Hub.
* Una vez descargada creó un nuevo contenedor de la imagen y ejecutó el mensaje que se mostró en la consola "Hello from Docker!"
* El demonio de docker detectó esta salida y la re-dirigió a la salida de la consola de docker client (nuestra consola)

### [< Volver atrás](../../README.md)